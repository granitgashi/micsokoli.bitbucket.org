var inputs = new Object();
var inputsArray = new Array();
$(document).ready(function () {
	$("#create-table-button").click(function () {
		createBlankTable();
	});
});

function createBlankTable() {
	var numberOfRows = $("#table-rows").val();
	var numberOfColumns = $("#table-columns").val();

	var blankTableCellsHTML = "";

	for (var rows = 0; rows < numberOfRows; rows++) {
		blankTableCellsHTML += "<tr>";
		for (var columns = 0; columns < numberOfColumns; columns++) {
			blankTableCellsHTML += "<td>";
			blankTableCellsHTML += "<input type='text' class='form-control'></input>";
			blankTableCellsHTML += "</td>";
		}
		blankTableCellsHTML += "</tr>";
	}
	var blankTableHTML = "" + 
		"<table id='raw-data-table' class='table'>" + 
		blankTableCellsHTML +
		"</table>" + 
		"<button id='analyze-button' class='btn btn-primary'>Analizo</button>" + 
		"<div id='highcharts-container' style='min-width: 500px; height: 400px; margin: 0 auto'></div>";
	$("#raw-data-table-container").html(blankTableHTML);
	$("#table-size-input-container").hide();

	$("#analyze-button").click(function () {
		analyze();
	});	
}

function analyze() {
	calculateFrequencies();
	drawChart();
}

function calculateFrequencies() {
	inputs = new Object();
	inputsArray = new Array();
	var currentValue = "";

	$("#raw-data-table tbody tr td input").each(function (key) {
		
		currentValue = $(this).val();
        if(currentValue != "") {
            if (inputs[currentValue] == undefined) {
                inputs[currentValue] = 1;
            }
            else {
                inputs[currentValue]++;
            } 
        }
		
	});
	for (index in inputs) {
		inputsArray.push([index, inputs[index]]);
	}
console.log(inputs);
console.log("inputsArray: " + eval(inputsArray));
}

function drawChart() {
	$(function () {
        $('#highcharts-container').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Analiza statistikore'
            },
            subtitle: {
                text: 'Shfaqja e vlerave të ndryshores X sipas dendurive'
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: 0,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'f(x)'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: 'Denduria {point.y:.1f}</b>',
            },
            series: [{
                name: 'Denduritë',
                data: inputsArray,
                dataLabels: {
                    enabled: true,
                    rotation: 0,
                    color: '#FFFFFF',
                    align: 'center',
                    x: 0,
                    y: 30,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif',
                        textShadow: '0 0 3px black'
                    }
                }
            }]
        });
    });
    

}